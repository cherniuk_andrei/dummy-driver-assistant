import cv2
import time
import sys
import misc
import recognition
import defines


def script():
    cap = cv2.VideoCapture(defines.video_path)
    width, height = misc.set_frame_size()
    out = cv2.VideoWriter(misc.out_path(defines.video_path, recognition.get_yolo_mode()), cv2.VideoWriter_fourcc('M','J','P','G'), 30, (width, height))

    init_roi = misc.set_roi(defines.video_path)
    if init_roi == None:
        print("No way to get reference_points!")
        return

    modified_roi = misc.roi_to_relative_form(init_roi, [width, height])
    delay_max = 0
    delay_min = float("inf")

    while True:
        tik = time.perf_counter()
        successfully_loaded, initial_image = cap.read()

        if successfully_loaded == False:
            print("Process finished or something happened with a video path")
            break

        initial_image = cv2.resize(initial_image, (width, height), None)

        try:
            # recognition.do_networks_things(initial_image, width, height)

            # roi_image = initial_image.copy() # copying here to avoid another one excess copy 
            # misc.visualize_roi(roi_image, init_roi)

            outlined_image = misc.edge_detection(initial_image)
            perspective_transformed_image = misc.perspective_warp(outlined_image, modified_roi, (width, height))

            sliding_window_image, recognized_lanes = misc.sliding_window(perspective_transformed_image.copy())
            final_image = misc.draw_lanes(initial_image, recognized_lanes, (width, height), modified_roi)
            
            
            # cv2.imshow("Initial image",  initial_image)
            # cv2.imshow("ROI image",  roi_image) 
            # cv2.imshow("Outlined image", outlined_image)            
            # cv2.imshow("Perspective transformation image",  perspective_transformed_image)
            # cv2.imshow("Sliding window image", sliding_window_image)
            # cv2.imshow("Final image",  final_image)            

        except:
            print("no lane were found!")
            if cv2.getWindowProperty("Final image", 0) >= 0:
                cv2.imshow("Final image",  initial_image)

            

        tok = time.perf_counter()
        # print(tok - tik)

        delay_min = min(delay_min, tok-tik)
        delay_max = max(delay_max, tok-tik)
        


        out.write(final_image)
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    f = open(defines.video_path + ".txt", "a")
    f.write("min = \n")
    f.write(str(delay_min) + "\n")
    f.write("max = \n")
    f.write(str(delay_max) + "\n")
    f.close()



    cap.release()
    out.release()

    cv2.destroyAllWindows()

def main(unused_command_line_args):
    for i in range(5):
        script()
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))