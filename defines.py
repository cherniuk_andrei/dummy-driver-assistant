import cv2
import numpy as np
from enum import IntEnum, Enum

class CannyThreshold(IntEnum):
    MINVAL = 50
    MAXVAL = 100

class YoloMode(Enum):
    HARD_YOLO = 1
    TINY_YOLO = 2

class Curve(IntEnum):
    LEFT = 0
    RIGHT = 1

class Params(IntEnum):
    WIDTH = 0
    HEIGHT = 1

ncurves = 2

#---------------------------------------------------------------
# yolo_mode = YoloMode.TINY_YOLO
yolo_mode = YoloMode.HARD_YOLO

# video_path = 'mainVideo/main_video_cutted.mp4'
# video_path = 'mainVideo/project_video.mp4'
# video_path = 'serpantine/serpantine.mp4'
# video_path = 'hardToRecognize/challenge_video.mp4'
video_path = 'russianRoad/russian_road.mp4'
# video_path = 'wideRoad/wide_road.mp4'
#---------------------------------------------------------------
font = cv2.FONT_HERSHEY_PLAIN

#bounding box color
pretty_blue_color  = [209, 52, 42]

#warning color
pretty_red_color   = [0, 0, 255]

#road color
pretty_green_color = (15, 200, 15)

#sliding windows cell color
cell_color 	  	   = (255, 255, 100)

#edge detection filter's colors
lower_yellow  	   = np.array([150, 120,  50])
upper_yellow  	   = np.array([255, 255,  100])

lower_white   	   = np.array([210, 210, 210])
upper_white   	   = np.array([255, 255, 255])

#string warning
warning = "DANGER!"

#default coordinaties of warp perspective
square_coordinates = np.float32([(0, 0), (1, 0), (0, 1), (1, 1)])