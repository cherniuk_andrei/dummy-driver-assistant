import cv2
import numpy as np
import math
import defines

#############################################################
#bounding box text font

def get_yolo_mode():
    return {
        defines.YoloMode.HARD_YOLO : "yolov3",
        defines.YoloMode.TINY_YOLO : "yolov3-tiny"
    }.get(defines.yolo_mode, "yolov3-tiny")

# Network's preprocessing
net_params = get_yolo_mode()
net = cv2.dnn.readNet("weights/" + net_params + ".weights", "cfg/" + net_params + ".cfg")


classes = []
with open("coco.names", "r") as f:
    classes = [line.strip() for line in f.readlines()]

layer_names = net.getLayerNames()


output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]


def get_distance(my_size, another_pos):
    return math.sqrt(pow(my_size[0] // 2 - (another_pos[0] + another_pos[2] / 2), 2) + pow(my_size[1] - (another_pos[1] + another_pos[3] / 2), 2))


def is_too_close(my_size, another_pos):
    dist = get_distance(my_size, another_pos)
    print(dist)
    return dist < 80


def show_warning(image, w, h, pts):
    textsize = cv2.getTextSize(defines.warning, defines.font, 3, 1)[0]
    # get coords based on boundary
    textX = (w//2  - int(textsize[0] * 0.5))
    textY = (int(h * 0.25) + textsize[1]) // 2
    cv2.putText(image, defines.warning, (textX, textY), defines.font, 3, defines.pretty_red_color, 1)
    
    cv2.circle(image,(int(w // 2),int(h - 20)),5,(39, 187, 87),cv2.FILLED) #another car
    cv2.circle(image,(int(pts[0] + pts[2] / 2),int(pts[1] + pts[3] / 2)),5,(0, 0, 255),cv2.FILLED) # my


def do_networks_things(image, width, height):
    global too_danger
    blob = cv2.dnn.blobFromImage(image, 0.001, (288, 288), (0, 0, 0), True, crop=False)
    net.setInput(blob)

    # Find them!
    outs = net.forward(output_layers)

    class_ids, confidences, boxes = [], [], []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.7:
                # Object detected
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                # Rectangle coordinates
                x = int(center_x - w / 2)
                y = int(center_y - h / 2)

                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)
    indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.8, 0.3)
    for i in range(len(boxes)):
        if i in indexes:
            x, y, w, h = boxes[i]
            label = str(classes[class_ids[i]])
            confidence = confidences[i]
            cv2.rectangle(image, (x, y), (x + w, y + h), defines.pretty_blue_color, 2)
            cv2.putText(image, label, (x, y), defines.font, 1, defines.pretty_blue_color, 1)
            if is_too_close((width, height), (x, y, w, h)):
                show_warning(image, width, height, (x,y, w, h))
