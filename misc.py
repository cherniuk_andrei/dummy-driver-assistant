import numpy as np
import cv2
import pickle
import defines


def out_path(filepath, yolo_mode):
    folder_name = filepath.split('.', 1)[0]
    return folder_name + "_" + yolo_mode + "_out.avi"



def roi_to_relative_form(reference_points, image):
    """
    { transfrorm reference point to relative form. Example: (150, 200) - value
    of a single point. (600, 400) - size of image Result:  [(150, 200), ...] /
    (600, 400) = [(1 / 4, 1 / 2), ...] <=> [(0.25, 0.5), ...] }
    
    :param      reference_points:  location of the point on the screen
    :type       reference_points:  { list of tuples }
    :param      image:             frame retrieved from the capture
    :type       image:             { cv2.Mat }
    
    :returns:   { reference points' values in a relative form. Every value is
                ranged(0.0, 1.0). }
    :rtype:     { list of tuples }
    """
    return np.float32([(int(point[defines.Params.WIDTH])  / image[defines.Params.WIDTH], 
                        int(point[defines.Params.HEIGHT]) / image[defines.Params.HEIGHT]) for point in reference_points])



def set_roi(filepath):
    """
    Sets the reference points of the image. IMPORTANT: Use it only in
    preprocessing stage! This data response for road lines position:
    
    [left_top_w, left_top_h]                    [right_top_w, right_top_h]
    
    [left_bottom_w, left_bottom_h]              [right_bottom_w, right_bottom_h]
    
    :param      filepath:  path to the source file for capture. Reference
                           points' values contains in the same folder
    :type       filepath:  { string }
    
    :returns:   { approximate coordinates of reference points for file }
    :rtype:     { list of tuples }
    """
    filename = filepath.split('/', 1)[0] + "/referencePoints.txt"
    try:
        with open(filename, 'r') as file:
            return [line.strip().split(',') for line in file.readlines()]
    except:
        return None
    


def visualize_roi(img, reference_points):
    """
    Draws reference points on the screen.
    
    :param      img:               undistorted frame retrieved from the capture
    :type       img:               { cv2.Mat }
    :param      reference_points:  location of the reference points on the
                                   window
    :type       reference_points:  { list of tuples }
    
    :returns:   { source image with outlied reference points }
    :rtype:     { cv2.Mat }
    """
    for x in range(0,4):
        cv2.circle(img,(int(reference_points[x][defines.Params.WIDTH]),int(reference_points[x][defines.Params.HEIGHT])),5,(39, 187, 87),cv2.FILLED)



def set_frame_size():
	"""
    Sets the frame's size.
    
    :returns:   { new size of the frame as width and height }
    :rtype:     { two ints }
    """
	with open("frameSize.txt", 'r') as file:
		line = file.readline()
		line = [int(i) for i in line.split(',')]
	return line[defines.Params.WIDTH], line[defines.Params.HEIGHT]



class Counter:
    """
    Counter object via the array of curves
    """ 
    def __init__(self):
        self.counter = 0

    def reset(self):
        self.counter = 0

    def value(self):
        return self.counter

    def increment(self):
        self.counter += 1

    def __ge__(self, other):
        """
        Greater-than-or-equal comparison operator of int and Counter class.
        
        :param      other:  some number
        :type       other:  { int }
        
        :returns:   The result of the greater-than-or-equal comparison on other
                    and self.counter
        :rtype:     { bool }
        """
        return self.counter >= other



def undistort_image(img, cal_dir='cal_pickle.p'):
    """
    Removing distortion from the image using parameters of the camera lense.
    Params img must by cv.Mat type. OpenCV function is applied!
    
    :param      img:      frame retrieved from the capture
    :type       img:      { cv2.Mat }
    :param      cal_dir:  location of the calibration file
    :type       cal_dir:  { string }
    
    :returns:   { Undistorted image. Distortion removing is based on lense
                characteristics }
    :rtype:     { cv.Mat }
    """

    with open(cal_dir, mode="rb") as f:
    # with open(defines.undistort_coeffs, mode="rb") as f:
        file = pickle.load(f)
    return cv2.undistort(img, file["mtx"], file["dist"], None, file["mtx"])



def apply_single_filter(color_format, lower_bound, upper_bound):
    return cv2.inRange(color_format, lower_bound, upper_bound)



def color_filter(img):
    """
    Color filter function. Aimed to retrieve white-black image with preprocessed
    yellow and white colors location.
    
    :param      img:  undistorted frame retrieved from the capture
    :type       img:  { cv2.Mat }
    
    :returns:   { logical sum of yellow and white colors position }
    :rtype:     { cv2.Mat }
    """
    rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    
    return cv2.bitwise_or(apply_single_filter(rgb, defines.lower_white,  defines.upper_white), \
                          apply_single_filter(rgb, defines.lower_yellow, defines.upper_yellow))



def edge_detection(img):
    """
    Gets the canvas of the image. Applies color filters to the image and and
    edge detection
    
    :param      img:  undistorted frame retrieved from the capture
    :type       img:  { cv2.Mat }
    
    :returns:   Three objects: combined modified edge-detected and
                color-filtered image, edge-detected image, color-filtered image
    :rtype:     { cv2.Mat, cv2.Mat, cv2.Mat }
    """
    img_gray  = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img_canny = cv2.Canny(img_gray, defines.CannyThreshold.MINVAL, defines.CannyThreshold.MAXVAL)

    return cv2.bitwise_or(img_canny, color_filter(img))



def perspective_warp(img, src, dst_size, dst=defines.square_coordinates):
    """
    { function_description }

    :param      img:       The image
    :type       img:       { type_description }
    :param      src:       The source
    :type       src:       { type_description }
    :param      dst_size:  The destination size
    :type       dst_size:  { type_description }
    :param      dst:       The destination
    :type       dst:       { type_description }

    :returns:   { description_of_the_return_value }
    :rtype:     { return_type_description }
    """
    src = src * np.float32([(img.shape[1],img.shape[0])])
    dst = dst * np.float32(dst_size)
    M = cv2.getPerspectiveTransform(src, dst)
    return cv2.warpPerspective(img, M, dst_size)



def inv_perspective_warp(img, dst_size, dst, src=defines.square_coordinates):
    """
    { function_description }

    :param      img:       The image
    :type       img:       { type_description }
    :param      dst_size:  The destination size
    :type       dst_size:  { type_description }
    :param      dst:       The destination
    :type       dst:       { type_description }
    :param      src:       The source
    :type       src:       { type_description }

    :returns:   { description_of_the_return_value }
    :rtype:     { return_type_description }
    """
    return perspective_warp(img, src, dst_size, dst)



def get_hist(img):
    """
    Gets the hist.

    :param      img:  The image
    :type       img:  { type_description }

    :returns:   The hist.
    :rtype:     { return_type_description }
    """
    return np.sum(img[img.shape[0]//2:,:], axis=0)



def in_range(value, min, max):
    """
    { function_description }

    :param      value:  The value
    :type       value:  { type_description }
    :param      min:    The minimum
    :type       min:    { type_description }
    :param      max:    The maximum
    :type       max:    { type_description }

    :returns:   { description_of_the_return_value }
    :rtype:     { return_type_description }
    """
    return (value >= min) & (value < max) 



def extract_curves_from_image(image):
    """
    { function_description }

    :param      image:  The image
    :type       image:  { type_description }

    :returns:   { description_of_the_return_value }
    :rtype:     { return_type_description }
    """
    histogram = get_hist(image)

    # finding position of the left and right curves
    middle_point    = int(histogram.shape[0] / 2)

    #max point at left part of the frame and max point at right part
    return np.array([np.argmax(histogram[:middle_point]), np.argmax(histogram[middle_point + 30:]) + middle_point])

def process_single_cell(its_curve, margin, out_img, win_iterators,
    nonzeroy, nonzerox, left_lane_inds, right_lane_inds):
    """
    { function_description }

    :param      its_curve:        The its curve
    :type       its_curve:        { type_description }
    :param      margin:           The margin
    :type       margin:           { type_description }
    :param      out_img:          The out image
    :type       out_img:          { type_description }
    :param      win_iterators:    The window iterators
    :type       win_iterators:    { type_description }
    :param      nonzeroy:         The nonzeroy
    :type       nonzeroy:         { type_description }
    :param      nonzerox:         The nonzerox
    :type       nonzerox:         { type_description }
    :param      left_lane_inds:   The left lane inds
    :type       left_lane_inds:   { type_description }
    :param      right_lane_inds:  The right lane inds
    :type       right_lane_inds:  { type_description }

    :returns:   { description_of_the_return_value }
    :rtype:     { return_type_description }
    """
    cell_x_beg = its_curve - margin
    cell_x_end = its_curve + margin

    # Draw the windows on the visualization image
    cv2.rectangle(out_img, (cell_x_beg, win_iterators[0]),
                           (cell_x_end, win_iterators[1]),
                           defines.cell_color, 1)


    # Identify the nonzero pixels in x and y within the window
    valid_cell = (in_range(nonzeroy, win_iterators[0], win_iterators[1]) 
                    & in_range(nonzerox, cell_x_beg, cell_x_end)).nonzero()[0]

    return valid_cell


POLYNOM_DEGREE = 1

left_curve_intepolation_coeffs_holder  = [[], []]
right_curve_intepolation_coeffs_holder = [[], []]


interpolation_coeffs_holder = [left_curve_intepolation_coeffs_holder, right_curve_intepolation_coeffs_holder]


def curves_to_array(curves, ploty):
    left = np.array([np.transpose(np.vstack([curves[0], ploty]))])
    right = np.array([np.flipud(np.transpose(np.vstack([curves[1], ploty])))])
    return np.hstack((left, right))

def sliding_window(img, nwindows=15, margin = 40):
    """
    { function_description }

    :param      img:       The image
    :type       img:       { type_description }
    :param      nwindows:  The nwindows
    :type       nwindows:  number
    :param      windows_width:    The windows width
    :type       windows_width:    number

    :returns:   { description_of_the_return_value }
    :rtype:     { return_type_description }
    """
    global interpolation_coeffs_holder
    # , interpolation_coeffs_holder1


    curves = extract_curves_from_image(img)

    window_height = int(img.shape[0] / nwindows)

    # Identify the x and y positions of all nonzero pixels in the image
    nonzero = img.nonzero()
    nonzeroy = np.array(nonzero[0])
    nonzerox = np.array(nonzero[1])

    # Create empty lists to retrieve pixel indices
    left_lane_inds = []
    right_lane_inds = []


    out_img = np.dstack((img, img, img))

    for window in range(nwindows):
        #definition of current windows h in the subset
        window_cell_y_begin = img.shape[0] - (window + 1) * window_height
        window_cell_y_end   = img.shape[0] - window * window_height

        valid_pixels = []
        valid_pixels.append(process_single_cell(curves[defines.Curve.LEFT], margin, out_img, (window_cell_y_begin, window_cell_y_end),
                                                nonzeroy, nonzerox, left_lane_inds, right_lane_inds))

        valid_pixels.append(process_single_cell(curves[defines.Curve.RIGHT], margin, out_img, (window_cell_y_begin, window_cell_y_end),
                                                nonzeroy, nonzerox, left_lane_inds, right_lane_inds))

        # # If we found at least 1 valid pixel, recenter next window on their mean position
        if len(valid_pixels[defines.Curve.LEFT]) != 0:
            left_lane_inds.append(valid_pixels[defines.Curve.LEFT])
            if len(valid_pixels[defines.Curve.LEFT]) > 1:
                curves[defines.Curve.LEFT] = np.int(np.mean(nonzerox[valid_pixels[defines.Curve.LEFT]]))
    
            right_lane_inds.append(valid_pixels[defines.Curve.RIGHT])
            if len(valid_pixels[defines.Curve.RIGHT]) > 1:
                curves[defines.Curve.RIGHT] = np.int(np.mean(nonzerox[valid_pixels[defines.Curve.RIGHT]]))


    # Concatenate the arrays of indices
    left_lane_inds  = np.concatenate(left_lane_inds)
    right_lane_inds = np.concatenate(right_lane_inds)

    leftx  = nonzerox[left_lane_inds]
    lefty  = nonzeroy[left_lane_inds]

    rightx = nonzerox[right_lane_inds]
    righty = nonzeroy[right_lane_inds]

    amount_of_curves = defines.ncurves

    if leftx.size != 0 and rightx.size != 0:

        curves_polynomial_coeffs = [np.polyfit(lefty,  leftx,  POLYNOM_DEGREE), np.polyfit(righty, rightx, POLYNOM_DEGREE)]

        # print("left curve equation  = \n",  np.poly1d(curves_polynomial_coeffs[defines.Curve.LEFT]))
        # print("right curve equation = \n",  np.poly1d(curves_polynomial_coeffs[defines.Curve.RIGHT]))

        for i in range (POLYNOM_DEGREE + 1):
            for curve in range(amount_of_curves):
                interpolation_coeffs_holder[curve][i].append(curves_polynomial_coeffs[curve][i])

        # Generate x and y values for plotting
        ploty = np.linspace(0, img.shape[0] - 1, img.shape[0])

        final_curves = [np.float(0), np.float(0)]
    
        for i in range (POLYNOM_DEGREE + 1):
            for curve in range(amount_of_curves):        
                adjusted_coefficient = np.mean(interpolation_coeffs_holder[curve][i][-nwindows:])
                final_curves[curve] += adjusted_coefficient * ploty ** (POLYNOM_DEGREE - i)

        temp_pts = np.array(curves_to_array(final_curves, ploty), np.int32)
        temp_pts = temp_pts.reshape((-1, 1, 2))
        cv2.polylines(out_img, temp_pts, True, defines.pretty_red_color, thickness=3)
        return out_img, final_curves
    else:
        return out_img, []


def draw_lanes(img, curves, size, src):
    """
    Draws lanes.
    
    :param      img:     The image
    :type       img:     { type_description }
    :param      curves:  The curves
    :type       curves:  { type_description }
    :param      size:    The size
    :type       size:    { type_description }
    :param      src:     The source
    :type       src:     { type_description }
    :param      left_fit:     The left fit
    :param      right_fit:    The right fit
    :param      frameWidth:   The frame width
    :param      frameHeight:  The frame height
    
    :returns:   { description_of_the_return_value }
    :rtype:     { return_type_description }
    """
    ploty = np.linspace(0, img.shape[0] - 1, img.shape[0])

    points = curves_to_array(curves, ploty)

    color_img = np.zeros_like(img)
    cv2.fillPoly(color_img, np.int_(points), defines.pretty_green_color)
    return cv2.addWeighted(img, 1, inv_perspective_warp(color_img, (size[defines.Params.WIDTH], size[defines.Params.HEIGHT]), src), 0.6, 0)